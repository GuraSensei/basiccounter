﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CounterLibrary;

namespace BasicCounterApp
{
    public partial class Form1 : Form
    {
        private CounterClass counter;
        public Form1()
        {
            InitializeComponent();
            this.counter = new CounterClass();
        }

        private void MoinsUnButton_Click(object sender, EventArgs e)
        {
            if (counter.getTotal() > 0)
            {
                counter.moinsUn();
                Total.Text = counter.getTotal().ToString();
            }
            else MessageBox.Show("Valeur negative impossible");
            
        }

        private void PlusUnButton_Click(object sender, EventArgs e)
        {
            counter.plusUn();
            Total.Text = counter.getTotal().ToString();
        }

        private void RazButton_Click(object sender, EventArgs e)
        {
            counter.raz();
            Total.Text = counter.getTotal().ToString();
        }
    }
}
