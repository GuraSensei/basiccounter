﻿namespace BasicCounterApp
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.PlusUnButton = new System.Windows.Forms.Button();
            this.MoinsUnButton = new System.Windows.Forms.Button();
            this.razButton = new System.Windows.Forms.Button();
            this.Total = new System.Windows.Forms.Label();
            this.TotalLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // PlusUnButton
            // 
            this.PlusUnButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.PlusUnButton.Location = new System.Drawing.Point(486, 184);
            this.PlusUnButton.Name = "PlusUnButton";
            this.PlusUnButton.Size = new System.Drawing.Size(75, 46);
            this.PlusUnButton.TabIndex = 0;
            this.PlusUnButton.Text = "+";
            this.PlusUnButton.UseVisualStyleBackColor = true;
            this.PlusUnButton.Click += new System.EventHandler(this.PlusUnButton_Click);
            // 
            // MoinsUnButton
            // 
            this.MoinsUnButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.MoinsUnButton.Location = new System.Drawing.Point(201, 184);
            this.MoinsUnButton.Name = "MoinsUnButton";
            this.MoinsUnButton.Size = new System.Drawing.Size(75, 46);
            this.MoinsUnButton.TabIndex = 1;
            this.MoinsUnButton.Text = "-";
            this.MoinsUnButton.UseVisualStyleBackColor = true;
            this.MoinsUnButton.Click += new System.EventHandler(this.MoinsUnButton_Click);
            // 
            // razButton
            // 
            this.razButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.razButton.Location = new System.Drawing.Point(354, 250);
            this.razButton.Name = "razButton";
            this.razButton.Size = new System.Drawing.Size(75, 52);
            this.razButton.TabIndex = 2;
            this.razButton.Text = "RAZ";
            this.razButton.UseVisualStyleBackColor = true;
            this.razButton.Click += new System.EventHandler(this.RazButton_Click);
            // 
            // Total
            // 
            this.Total.AutoSize = true;
            this.Total.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.Total.Location = new System.Drawing.Point(350, 194);
            this.Total.Name = "Total";
            this.Total.Size = new System.Drawing.Size(20, 24);
            this.Total.TabIndex = 3;
            this.Total.Text = "0";
            // 
            // TotalLabel
            // 
            this.TotalLabel.AutoSize = true;
            this.TotalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.TotalLabel.Location = new System.Drawing.Point(350, 131);
            this.TotalLabel.Name = "TotalLabel";
            this.TotalLabel.Size = new System.Drawing.Size(51, 24);
            this.TotalLabel.TabIndex = 4;
            this.TotalLabel.Text = "Total";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 421);
            this.Controls.Add(this.TotalLabel);
            this.Controls.Add(this.Total);
            this.Controls.Add(this.razButton);
            this.Controls.Add(this.MoinsUnButton);
            this.Controls.Add(this.PlusUnButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button PlusUnButton;
        private System.Windows.Forms.Button MoinsUnButton;
        private System.Windows.Forms.Button razButton;
        private System.Windows.Forms.Label Total;
        private System.Windows.Forms.Label TotalLabel;
    }
}

